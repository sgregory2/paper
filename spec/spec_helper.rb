require_relative '../src/paper'

Paper.configure do |config|
  config.root = "#{__dir__}/fixtures"
end