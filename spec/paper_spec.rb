RSpec.describe Paper::Page do
  let(:klass) do
    Class.new do
      include Paper::Page

      view 'test.vue' do
        element :foo, id: 'foo' do
          elements :things, class: /^things/ do
            element :button, type: 'submit'
          end

          element :heading, class: 'heading'
        end
      end
    end
  end

  let(:invalid) do
    Class.new do
      include Paper::Page

      view 'test.vue' do
        element :foo, id: 'foo' do
          elements :things, class: /^things/ do
            element :link, href: 'submit'
          end

          element :heading, class: 'heading'
        end
      end
    end
  end

  let(:page) { klass.new }
  it 'should validate unknown elements' do
    expect(invalid.valid?).to be(false)
  end

  it 'should validate' do
    expect(klass.valid?).to be(true)
  end

  it 'should resolve to xpath' do
    expected = [{ id: 'foo'}, { class: /^things/, index: 2 }, { type: 'submit' }]
    expect(page.foo.things[2].button.location).to eql(expected)
  end
end