require_relative './validators/vue'

module Paper
  class Config
    attr_writer :root
    def initialize
      @root
    end

    def root
      @root || "./"
    end
  end

  def self.config
    @config || Config.new
  end

  def self.configure(&block)
    @config = Config.new
    block.call(@config)
  end

  module ElementHelper
    def element(name, **locator, &block)
      element = Element.new(self, name, **locator)
      element.instance_eval(&block) if block
      @children << element
    end

    def elements(name, **locator, &block)
      elements = Elements.new(self, name, **locator)
      elements.instance_eval(&block) if block
      @children << elements
    end

    def location
      return [locator] if parent.is_a? View
      parent.location.concat [locator]
    end

    def parent
      @context
    end
  end

  class Elements
    attr_reader :name, :locator, :context, :children

    def initialize(context, name, **locator)
      @context = context
      @name = name
      @locator = locator
      @children = []
    end


    def element(name, **locator, &block)
      element = Element.new(self, name, **locator)
      element.instance_eval(&block) if block
      @children << element
    end

    def location
      return [locator] if parent.is_a? View
      parent.location.concat locator
    end

    def parent
      @context
    end

    def [](index)
      element = Element.new(parent, name, **locator.merge(index: index))
      element.children = children
      element.children.each do |child|
        child.context = element
      end
      return element
    end
  end

  class Element
    attr_accessor :name, :locator, :context, :children

    include ElementHelper

    def initialize(context, name, **locator)
      @context = context
      @name = name
      @locator = locator
      @children = []
    end

    def method_missing(method, hargs = {})
      found = find_element(method)
      return found if found
      raise ArgumentError, "No element for #{name} with name #{method}"
    end

    private

    def find_element(method)
      @children.find do |element|
        element.name == method
      end
    end

    def find_parent_element(method)
      context.children.find do |element|
        element.name == method
      end
    end
  end

  class View
    attr_reader :children

    include ElementHelper

    def initialize(file_name)
      @file_name = file_name
      @children = []
    end
  end

  module PageMethods
    def view(file, &block)
      @views ||= {}
      view = View.new(file)
      view.instance_eval(&block) if block
      @views[file] = view
    end

    def views
      @views
    end

    def validate
      @views.reduce([]) do |errors, (file, view)|
        errors.concat Validators::Vue.validate(file, view) if file =~ /\.vue$/
        errors
      end
    end

    def valid?
      validate.empty?
    end
  end


  module Page
    def self.included(klass)
      klass.extend PageMethods
    end

    def initialize
      super
      self.class.views.each do |_, view|
        view.children.each do |element|
          self.class.send(:define_method,  element.name) do
            element
          end
        end
      end
    end
  end
end