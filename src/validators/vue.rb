require 'strscan'

module Paper
  module Validators
    class Parser
      attr_reader :tokens
      attr_accessor :position
      def self.parse(str)
        new(Lexer.tokenize(str)).parse
      end

      def initialize(tokens)
        @tokens = tokens
        @position = 0
        @data = { template: [] }
      end

      # returns
      # { tag: 'div id=foo', children: [] }
      def parse
        expect(:START_TEMPLATE)
        tags = repeat(:parse_tags)
        expect(:END_TEMPLATE)
        tags
      end

      def parse_tags
        tag = {}
        if tokens = expect(:START_TAG, :TAG_CONTENT)
          tag[:tag] = tokens[1][1]
          tag[:children] = repeat(:parse_tags).reject(&:nil?)
          tag
        elsif expect(:END_TAG)
          nil
        end
      end

      def repeat(method)
        results = []

        while result = send(method)
          results << result
        end

        results
      end

      def expect(*expected_tokens)
        upcoming = tokens[position, expected_tokens.size]
        if upcoming.map(&:first) == expected_tokens
          advance(expected_tokens.size)
          upcoming
        end
      end

      def need(*required_tokens)
        upcoming = tokens[position, required_tokens.size]
        expect(*required_tokens) or raise "Unexpected tokens. Expected #{required_tokens.inspect} but got #{upcoming.inspect}"
      end

      def advance(offset = 1)
        @position += offset
      end
    end

    class Lexer
      attr_reader :scanner
      attr_accessor :tokens

      def self.tokenize(str)
        new(str).tokenize
      end

      def initialize(str, scanner: StringScanner.new(str))
        @scanner = scanner
        @stack = []
        @tokens = []
      end

      def tokenize
        until scanner.eos?
          case state
          when :default
            if scanner.scan /.*<template>/
              tokens << [:START_TEMPLATE]
              push_state :start_template
            else
              advance
            end
          when :start_template
            if scanner.scan /<\/template>/
              tokens << [:END_TEMPLATE]
              pop_state
            elsif scanner.scan /\/.*>/
              tokens << [:END_TAG]
            elsif scanner.scan /<(?!\/)/
              tokens << [:START_TAG]
              push_state :start_tag
            else
              advance
            end
          when :start_tag
            # match <foobar> or <foobar/>
            content = scanner.scan_until />|\/>/
            tokens << [:TAG_CONTENT, content[0...-1]]
            pop_state
          end
        end
        tokens
      end

      def advance
        scanner.pos = scanner.pos + 1
      end

      def state
        @stack.last || :default
      end

      def push_state(state)
        @stack << state
      end

      def pop_state
        @stack.pop
      end

    end

    class Vue
      def self.validate(file, view)
        component = File.read "#{Paper.config.root}/#{file}"
        ast = Parser.parse(component)
        view.children.reduce([]) do |errors, element|
          location_error = validate_element(ast, element)
          errors.concat location_error unless location_error.empty?
          errors
        end
      end

      private

      def self.validate_element(ast, element, errors = [])
        found = find_ast(ast, element)
        if found
          element.children.each do |child|
            errors.concat validate_element(found[:children], child, errors)
          end
        else
          errors << "cannot find #{element.name}"
        end
        errors
      end

      def self.match_all?(ast, elements)
        elements.all? do |element|
          match_any?(ast, element)
        end
      end

      def self.find_ast(ast, element)
        ast.find do |tag|
          match?(element, tag)
        end
      end

      def self.match?(element, tag)
        element.locator.all? do |(k, v)|
          regex = v.is_a?(Regexp) ? v : /#{v}/
          value = tag[:tag].split(/#{k}="?'?`?/)[1]
          value && !regex.match(value).nil?
        end
      end
    end
  end
end